import os

curr_path = os.path.dirname(os.path.abspath(__file__))
curr_path = '/'.join(curr_path.split('/')[:-2])
per_utt = 'infrared_cn_asr/exp/tri4b_ali/decode_train_word.si/scoring_kaldi/wer_details/per_utt'
per_utt = curr_path+'/'+ per_utt

refs = []
hyps = []
wavs = []
txt_lines = []

with open(per_utt,'r')as f:
    lines = f.readlines()
    for line in lines:
        if 'ref' in line:
            ref_txt = ''.join(line.strip().split(' ')[2:])
            refs.append(ref_txt)
        if 'hyp' in line:
            hyp_txt = ''.join(line.strip().split(' ')[2:])
            hyps.append(hyp_txt)
        if '#csid' in line:
            wav_id = ''.join(line.strip().split(' ')[0])
            wavs.append(wav_id)


    for i in range(len(hyps)):
        txt_line = wavs[i]+' '+hyps[i]+' '+refs[i]
        txt_lines.append(txt_line)


corr_utt=[]
for i in range(len(txt_lines)):
    h_txt = txt_lines[i].split(' ')[1]
    r_txt = txt_lines[i].split(' ')[2]
    if h_txt == r_txt:
        corr_utt.append('_'.join(txt_lines[i].split(' ')[0].split('_')[:3]))

corr_utt=list(set(corr_utt))
for i in txt_lines:
    curr_utt = '_'.join(i.split(' ')[0].split('_')[:3])
    if curr_utt not in corr_utt:
        print(i)

