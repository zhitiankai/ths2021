import os
import re
import jieba
import sys



def get_text(path):
    jieba.set_dictionary('/work4/zhitiankai/infrared_cn_asr/local/words.txt')

    # path = '/work4/shenjingxin/data/InfraSpeech'
    spk_list = sorted(os.listdir(path))

    for spk in spk_list:
        txt_path = os.path.join(path,spk,'utt_list_utf8.txt')
        with open(txt_path) as f:
            txt = f.readlines()
        utt_list = os.listdir(os.path.join(path,spk))
        utt_list.remove('utt_list_utf8.txt')
        utt_list.remove('utt_list.txt')

        # utt_list.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))

        for utt in sorted(utt_list):
            utt_within_list = os.listdir(os.path.join(path,spk,utt))
            if 'camera.avi' in utt_within_list:
                utt_within_list.remove('camera.avi')
            if 'infared.avi' in utt_within_list:
                utt_within_list.remove('infared.avi')

            # utt_within_list.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))

            for i in txt:
                if utt.split('_')[1] == i.split(' ')[0]:
                    for j in sorted(utt_within_list):
                        if int(utt.split('_')[1])>= 141 and int(utt.split('_')[1])<=200:
                            no_jieba_txt = (' ').join(i.strip().split(' ')[1:])
                            rule = re.compile(u"[^a-zA-Z0-9\u4e00-\u9fa5]")
                            no_jieba_txt = rule.sub('',no_jieba_txt)
                            
                            jieba_txt = " ".join(jieba.cut(no_jieba_txt, False, False))
                            print(jieba_txt)

if __name__ == '__main__':
    path = sys.argv[1]
    get_text(path)