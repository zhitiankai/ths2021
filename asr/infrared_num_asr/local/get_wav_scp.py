import os
import sys


def get_wav_scp(path):
    spk_list = sorted(os.listdir(path))

    for spk in spk_list:
        txt_path = os.path.join(path,spk,'utt_list_utf8.txt')
        with open(txt_path) as f:
            txt = f.readlines()
        utt_list = os.listdir(os.path.join(path,spk))
        utt_list.remove('utt_list_utf8.txt')
        utt_list.remove('utt_list.txt')


        for utt in utt_list:
            utt_within_list = os.listdir(os.path.join(path,spk,utt))
            if 'camera.avi' in utt_within_list:
                utt_within_list.remove('camera.avi')
            if 'infared.avi' in utt_within_list:
                utt_within_list.remove('infared.avi')
            
            
            for i in txt:
                if utt.split('_')[1] == i.split(' ')[0]:
                    for j in utt_within_list:

                        if int(utt.split('_')[1])>= 1 and int(utt.split('_')[1])<=80:
                            print(spk+'_'+utt.split('_')[1]+'_'+j.split('.')[0]+' '+os.path.join(path,spk,utt,j))


if __name__ == '__main__':

    path = sys.argv[1]
    get_wav_scp(path)
