#!/bin/bash
. ./cmd.sh
. ./path.sh
n=12      #parallel jobs
stage=1
corpus_path=/work4/shenjingxin/data/InfraSpeech
echo stage $stage

#1.prepare data, generate wav.scp,utt2spk,spk2utt
if [ $stage -eq 1 ];then
    echo "######Begin prepare data!!!######"
    rm -rf data/train && mkdir data/train

    python local/get_wav_scp.py $corpus_path >data/train/wav.scp || exit 1;

    python local/get_utt2spk.py $corpus_path >data/train/utt2spk || exit 1;

    python local/get_txt.py $corpus_path >data/train/text || exit 1;
 
    for x in train; do
        utils/utt2spk_to_spk2utt.pl <data/$x/utt2spk >data/$x/spk2utt
    done
    echo "######Success data prepare!!!######"
fi

#2.generate mfcc fetures
if [ $stage -eq 2 ];then
    echo "######Begin generate mfcc fetures!!!######"
    rm -rf data/mfcc && mkdir -p data/mfcc &&  cp -R data/train data/mfcc && cp -R data/test data/mfcc
    for x in train; do
        utils/fix_data_dir.sh data/mfcc/$x || exit 1;
        steps/make_mfcc.sh --nj $n --cmd "$train_cmd" data/mfcc/$x exp/make_mfcc/$x mfcc/$x || exit 1;
        steps/compute_cmvn_stats.sh data/mfcc/$x exp/mfcc_cmvn/$x mfcc/$x || exit 1;
    done
    echo "######Success generate mfcc fetures!!!######"
fi

#3.prepare language
if [ $stage -eq 3 ];then
    echo "######Begin language!!!######"
    python local/get_language_txt.py  $corpus_path language_text || exit 1;
    ./run_lm.sh language_text 6 infrared_alp data/dict/words.txt || exit 1;
    tar -zcvf infrared_alp.6gram.tar.gz  infrared_alp.6.lm || exit 1;
    utils/prepare_lang.sh --position_dependent_phones false data/dict "<SPOKEN_NOISE>" data/local/lang data/lang || exit 1;
    utils/format_lm.sh data/lang infrared_alp.6gram.tar.gz data/dict/lexiconp.txt data/graph/lang || exit 1;
    
    echo "######Success language!!!######"
fi

#4.train model
if [ $stage -eq 4 ];then
    #monophone
    utils/fix_data_dir.sh data/mfcc/train || exit 1;
    echo "######Begin mono train######"
    steps/train_mono.sh --boost-silence 1.25 --nj $n --cmd "$train_cmd" data/mfcc/train data/lang exp/mono || exit 1;
    #monophone_ali
    steps/align_si.sh --boost-silence 1.25 --nj $n --cmd "$train_cmd" data/mfcc/train data/lang exp/mono exp/mono_ali || exit 1;
    echo "######Success mono train######"

    #triphone
    echo "######Begin triphone train######"
    steps/train_deltas.sh --boost-silence 1.25 --cmd "$train_cmd" 4000 20000 data/mfcc/train data/lang exp/mono_ali exp/tri1 || exit 1;
    #triphone_ali
    steps/align_si.sh --nj $n --cmd "$train_cmd" data/mfcc/train data/lang exp/tri1 exp/tri1_ali || exit 1;
    echo "######Success triphone train######"
    
    #lda_mllt
    echo "######Begin lda_mllt train######"
    steps/train_lda_mllt.sh --cmd "$train_cmd"  4500 25000 data/mfcc/train data/lang exp/tri1_ali exp/tri2b || exit 1;
    #lda_mllt_ali
    steps/align_si.sh  --nj $n --cmd "$train_cmd" --use_graphs true data/mfcc/train data/lang exp/tri2b exp/tri2b_ali || exit 1;
    echo "######Success lda_mllt train######"

    #sat
    echo "######Begin sat train######"
    steps/train_sat.sh --cmd "$train_cmd" 4500 25000 data/mfcc/train data/lang exp/tri2b_ali exp/tri3b ||  exit 1;
    #sat_ali
    steps/align_fmllr.sh --nj $n --cmd "$train_cmd" data/mfcc/train data/lang exp/tri3b exp/tri3b_ali || exit 1;
    echo "######Success sat train######"

    #quick
    echo "######Begin final train######"
    steps/train_quick.sh --cmd "$train_cmd" 8200 60000 data/mfcc/train data/lang exp/tri3b_ali exp/tri4b || exit 1;
    #quick_ali
    steps/align_fmllr.sh --nj $n --cmd "$train_cmd" data/mfcc/train data/lang exp/tri4b exp/tri4b_ali || exit 1;
    echo "######Success train end!!!######"
fi

#5.Decode model
if [ $stage -eq 5 ];then
    echo "######Begin decode######"
    local/infrared_alp_decode.sh --nj $n "steps/decode_fmllr.sh" exp/tri4b_ali data/mfcc || exit 1;
    for x in exp/tri4b_ali/decode_test_word; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
    echo "######Success decode !!!######"
fi

#6.Get check text
if [ $stage -eq 6 ];then
    echo "######Getting check text######"
    python3 local/get_check_txt.py > check.txt|| exit 1;
    echo "######Success get check text!!!######"
fi