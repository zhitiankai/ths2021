import re
import argparse
# path0 = '/work4/zhitiankai/infrared_data_format/clean_list/cam_clean_list'
# path1 = '/work4/zhitiankai/infrared_data_format/clean_list/inf_clean_list'
# path2 = '/work4/zhitiankai/infrared_data_format/clean_list/clean_allwav_list'
def gen_list(path0,path1,path2):
    with open(path0,'r')as f:
        cam_lines = f.readlines()

    with open(path1,'r')as f1:
        inf_lines = f1.readlines()

    tmp = []
    with open(path2,'r')as f2:
        wav_lines = f2.readlines()
    for i in wav_lines:
        j = i.split(' ')[0]
        tmp.append(j+'\n')
    wav_lines = tmp

    multi = list(set(cam_lines).intersection(inf_lines,wav_lines))
    multi.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))
    for z in multi:
        print(z.strip())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='get data soft link!!!')
    parser.add_argument("--cam_list_path", type=str, default='/work4/zhitiankai/infrared_data_format/clean_list/cam_clean_list')
    parser.add_argument("--inf_list_path", type=str, default='/work4/zhitiankai/infrared_data_format/clean_list/inf_clean_list')
    parser.add_argument("--wav_list_path", type=str, default='/work4/zhitiankai/infrared_data_format/clean_list/clean_allwav_list')
    args = parser.parse_args()

    gen_list(args.cam_list_path,args.inf_list_path,args.wav_list_path)
