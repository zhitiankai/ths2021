. ./path.sh
stage=1
echo stage $stage
#1.gennarate clean list
if [ $stage -eq 0 ];then
    echo "######Getting inrared/camera/wav list and mutilmodal list######"
    python3 gen_avi_list.py --path /work4/shenjingxin/data/InfraSpeech --filter filter/camera_filter_file.txt --judge cam --dst_file clean_list/cam_clean_list || exit 1;
    python3 gen_avi_list.py --path /work4/shenjingxin/data/InfraSpeech --filter filter/infrared_filter_file.txt --judge inf --dst_file clean_list/inf_clean_list || exit 1;
    python3 gen_utt_list.py --path /work4/shenjingxin/data/InfraSpeech --filter filter/wav_filter_file.txt --judge num --dst_file clean_list/clean_num_list || exit 1;
    python3 gen_utt_list.py --path /work4/shenjingxin/data/InfraSpeech --filter filter/wav_filter_file.txt --judge alp --dst_file clean_list/clean_alp_list || exit 1;
    python3 gen_utt_list.py --path /work4/shenjingxin/data/InfraSpeech --filter filter/wav_filter_file.txt --judge cn --dst_file clean_list/clean_cn_list || exit 1;    
    cat clean_list/clean_num_list clean_list/clean_alp_list clean_list/clean_cn_list >clean_list/clean_all_wav_list
    python3 gen_multi.py --cam_list_path clean_list/cam_clean_list --inf_list_path clean_list/inf_clean_list --wav_list_path clean_list/clean_all_wav_list > clean_list/mutilmodal_list
    echo "######Success get list!!!######"
fi

#2.generate clean data dir
if [ $stage -eq 1 ];then
    echo "######Getting clean data dir######"
    python3 ln_avi_data.py --src_path /work4/shenjingxin/data/img_infrared --clean_path clean_list/mutilmodal_list --judge camera --dst_path camera_img || exit 1;
    python3 ln_avi_data.py --src_path /work4/shenjingxin/data/img_infrared --clean_path clean_list/mutilmodal_list --judge infrared --dst_path infrared_img || exit 1;
    python3 ln_wav_data.py --src_path /work4/shenjingxin/data/InfraSpeech --clean_path clean_list/mutilmodal_list --dst_path speech_data || exit 1;

    echo "######Success clean data dir!!!######"
fi