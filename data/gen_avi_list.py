from genericpath import exists
import os
import re
import argparse

def get_utt_list(path):
    spk_list = sorted(os.listdir(path))
    spk_list.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))
    camera_lines = []
    infra_lines = []

    for spk in spk_list:
        utt_list = os.listdir(os.path.join(path,spk))
        utt_list.remove('utt_list_utf8.txt')
        utt_list.remove('utt_list.txt')

        utt_list.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))

        for utt in utt_list:
            avi_path = os.path.join(path,spk,utt)
            within_avi_list = os.listdir(avi_path)
            if 'camera.avi' in within_avi_list:
                save_num_line = spk+'_'+utt.split('_')[1]
                camera_lines.append(save_num_line)
            if 'infared.avi' in within_avi_list:
                save_num_line = spk+'_'+utt.split('_')[1]
                infra_lines.append(save_num_line)

    return camera_lines,infra_lines

def filter_lines(lines_list,filter_list_path,dst_file):
    dst_dir = '/'.join(dst_file.split("/")[:-1])
    if not os.path.exists(dst_dir):
        os.mkdir(dst_dir)

    with open(filter_list_path,'r')as f:
        filter_list = f.readlines()

    new_filter_list = []
    for i in filter_list:
        if '-' in i:
            spk_i = '_'.join(i.split('_')[:2])
            begin = int(i.split('_')[-1].split('-')[0]) 
            end = int(i.split('_')[-1].split('-')[1])
            for j in range(begin,end+1):
                new_filter_list.append(spk_i+'_'+str(j))
        else:
            new_filter_list.append(i.strip())

    for i in lines_list:
        if i not in new_filter_list:
            with open(dst_file,'a')as f:
                f.write(i+'\n')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='get claen avi_list!')
    parser.add_argument("--path", type=str, default="/work4/shenjingxin/data/InfraSpeech")
    parser.add_argument("--filter", type=str, default="/work4/zhitiankai/infrared_data_format/filter/infrared_filter_file.txt")
    parser.add_argument("--judge", type=str, default="inf")
    parser.add_argument("--dst_file", type=str, default="/work4/zhitiankai/infrared_data_format/clean_list/inf_clean_list")
    args = parser.parse_args()

    camera_lines,infra_lines = get_utt_list(args.path)
    if args.judge == 'inf':
        filter_lines(infra_lines,args.filter,args.dst_file)

    elif args.judge == 'cam':
        filter_lines(camera_lines,args.filter,args.dst_file)

    else:
        print("Warning!!!please input formal judge!")
