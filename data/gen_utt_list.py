import os
import sys
import re
import argparse

def get_utt_list(path):
    spk_list = sorted(os.listdir(path))
    spk_list.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))
    cn_lines = []
    num_lines = []
    alp_lines = []
    for spk in spk_list:
        txt_path = os.path.join(path,spk,'utt_list_utf8.txt')
        with open(txt_path) as f:
            txt = f.readlines()
        utt_list = os.listdir(os.path.join(path,spk))
        utt_list.remove('utt_list_utf8.txt')
        utt_list.remove('utt_list.txt')

        utt_list.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))
        for utt in utt_list:
            wavs = []
            wav_path = os.path.join(path,spk,utt)
            within_wav_list = os.listdir(wav_path)
            for wav in within_wav_list:
                wavs.append(wav.split('.')[-1])

            if 'wav' in wavs:
                for i in txt:
                    if utt.split('_')[1] == i.split(' ')[0]:
                        if int(utt.split('_')[1])>= 1 and int(utt.split('_')[1])<=80:
                            no_jieba_txt = (' ').join(i.strip().split(' ')[1:])
                            save_num_line = spk+'_'+utt.split('_')[1]+' '+no_jieba_txt
                            num_lines.append(save_num_line)
                            # print(spk+'_'+utt.split('_')[1]+' '+no_jieba_txt)

                        elif int(utt.split('_')[1])>= 141 and int(utt.split('_')[1])<=200:
                            no_jieba_txt = (' ').join(i.strip().split(' ')[1:])
                            rule = re.compile(u"[^a-zA-Z0-9\u4e00-\u9fa5]")
                            no_jieba_txt = rule.sub('',no_jieba_txt)
                            save_cn_line = spk+'_'+utt.split('_')[1]+' '+no_jieba_txt
                            cn_lines.append(save_cn_line)
                            # print(spk+'_'+utt.split('_')[1]+' '+no_jieba_txt)
                        
                        elif int(utt.split('_')[1])>= 81 and int(utt.split('_')[1])<=140:
                            no_jieba_txt = (' ').join(i.strip().split(' ')[1:])
                            save_alp_line = spk+'_'+utt.split('_')[1]+' '+no_jieba_txt
                            alp_lines.append(save_alp_line)
                            # print(spk+'_'+utt.split('_')[1]+' '+no_jieba_txt)

    return num_lines,alp_lines,cn_lines

def filter_lines(lines_list,filter_list_path,dst_file):
    dst_dir = '/'.join(dst_file.split("/")[:-1])
    if not os.path.exists(dst_dir):
        os.mkdir(dst_dir)

    with open(filter_list_path,'r')as f:
        filter_list = f.readlines()

    new_filter_list = []
    for i in filter_list:
        if '-' in i:
            spk_i = '_'.join(i.split('_')[:2])
            begin = int(i.split('_')[-1].split('-')[0]) 
            end = int(i.split('_')[-1].split('-')[1])
            for j in range(begin,end+1):
                new_filter_list.append(spk_i+'_'+str(j))
        else:
            new_filter_list.append(i.strip())

    for i in lines_list:
        if i.split(' ')[0] not in new_filter_list:
            with open(dst_file,'a')as f:
                f.write(i+'\n')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='get claen utt_list!')
    parser.add_argument("--path", type=str, default="/work4/shenjingxin/data/InfraSpeech")
    parser.add_argument("--filter", type=str, default="/work4/zhitiankai/infrared_data_format/filter/wav_filter_file.txt")
    parser.add_argument("--judge", type=str, default="cn")
    parser.add_argument("--dst_file", type=str, default="/work4/zhitiankai/infrared_data_format/clean_list/clean_cn_utt_list")
    args = parser.parse_args()

    num,alp,cn = get_utt_list(args.path)
    if args.judge == 'num':
        filter_lines(num,args.filter,args.dst_file)

    elif args.judge == 'alp':
        filter_lines(alp,args.filter,args.dst_file)
    
    elif args.judge == 'cn':
        filter_lines(cn,args.filter,args.dst_file)

    else:
        print("Warning!!!please input formal judge!")
