import os
import numpy as np
from argparse import ArgumentParser
import cv2

def check_avi(infrared_path,saveimg_path):
    spks2utt = {}
    spks = os.listdir(infrared_path)
    
    if not os.path.exists(saveimg_path) :
        os.makedirs(saveimg_path)

    for spk in spks:
        if os.path.isdir(os.path.join(infrared_path,spk)):
            spk_within_list =  os.listdir(os.path.join(infrared_path,spk))
            spk_within_list.remove('utt_list.txt')
            spk_within_list.remove('utt_list_utf8.txt')
            spks2utt[spk] = spk_within_list
        else:
            continue
    
    for spk in spks2utt:
        if not os.path.exists(os.path.join(saveimg_path,spk)) :
            os.makedirs(os.path.join(saveimg_path,spk))
        for utt in spks2utt[spk]:
            if not os.path.exists(os.path.join(saveimg_path,spk,utt)) :
                os.makedirs(os.path.join(saveimg_path,spk,utt))

            avi_list = os.listdir(os.path.join(infrared_path,spk,utt))

            for avi in avi_list:
                if avi.split('.')[0] == 'infared':
                    if not os.path.exists(os.path.join(saveimg_path,spk,utt,'infrared')) :
                        os.makedirs(os.path.join(saveimg_path,spk,utt,'infrared'))
                    avipath = os.path.join(infrared_path,spk,utt,avi)
                    avi2img(avipath,os.path.join(saveimg_path,spk,utt,'infrared'))
                elif avi.split('.')[0] == 'camera':
                    if not os.path.exists(os.path.join(saveimg_path,spk,utt,'camera')) :
                        os.makedirs(os.path.join(saveimg_path,spk,utt,'camera'))
                    avipath = os.path.join(infrared_path,spk,utt,avi)
                    avi2img(avipath,os.path.join(saveimg_path,spk,utt,'camera'))
                else:
                    continue
        
    print("ok")

def avi2img(avi_path,save_path):
    videoCapture = cv2.VideoCapture()
    videoCapture.open(avi_path)
    frames = videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)
    for i in range(int(frames)):
        ret, frame = videoCapture.read()
        cv2.imwrite(save_path+"/"+avi_path.split('/')[-1].split('.')[0]+('%d.jpg'%i), frame)


if __name__ == '__main__': 
    parser = ArgumentParser()
    parser.add_argument('--src_path', help='infrared avi data path!', type=str, default="/work4/shenjingxin/data/InfraSpeech")
    parser.add_argument('--dst_path', help='save img path!', type=str, default="/work4/shenjingxin/data/img_infrared")
    args = parser.parse_args() 

    check_avi(args.src_path,args.dst_path)
