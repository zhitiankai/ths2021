import codecs
import os

infrared_data = '/work4/shenjingxin/data/InfraSpeech'
spk_dirs =sorted(os.listdir(infrared_data))

for spk in spk_dirs:
    print("spk: ",spk)
    spk_txt = os.path.join(infrared_data,spk,'utt_list.txt')
    with codecs.open(spk_txt,'rb') as f:
        lines = f.readlines()
        for line in lines:
            line = line.decode("gb2312",'ignore').encode('utf-8')
            line = line.decode('utf-8')
            line = line.strip('\x00\r\n')
            w_path= os.path.join(infrared_data,spk,'utt_list_utf8.txt')

            with open(w_path,'a')as wf:
                wf.write(line+'\n')
            print(line)