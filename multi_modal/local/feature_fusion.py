import numpy as np

def data_normlize(data):
    return (data - data.mean(0)) / data.std(0)

if __name__=="__main__":
    path0 = '/work4/zhitiankai/infrared_data_format/multi_modal_add_noise_npz/inf_img_noise_0.008.npz'
    path1 = '/work4/zhitiankai/infrared_data_format/multi_modal_modify_lab_npz/ResNet34_half_ASP_amsoftmax_256_embedding.npz'

    data = np.load(path0)
    data1 = np.load(path1)

    img = data['img_vec']
    wav = data1['vectors']
    # wav = data1['img_vec']
    # wav = data1['img_vec']
    labels = data['img_lab']

    nor_img = data_normlize(img)
    nor_wav = data_normlize(wav)

    cat_vec = np.hstack((nor_img,nor_wav))

    np.savez('/work4/zhitiankai/infrared_data_format/multi_modal_add_noise_npz/inf_img_noise_0.008_asp_am',vectors = cat_vec,labels= labels)