import acoustics
import soundfile
import numpy as np
import librosa

def gen_white_nosie(length,color):
    noise=acoustics.generator.noise(length,color=color)
    return noise


if __name__=="__main__":
    noise_wav_path = '/work4/zhitiankai/infrared_noise_vad/data/white_noise.wav'
    clean_wav_path = '/work4/zhitiankai/infrared_noise_vad/python_aug/test_wav/mic1.wav'
    noise = gen_white_nosie(600000,'white')
    soundfile.write(noise_wav_path, noise, 16000)
