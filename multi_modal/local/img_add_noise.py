#方法1
# #随机生成符合正态（高斯）分布的随机数，means,sigma为两个参数
# import numpy as np
# import cv2
# from numpy import shape
# import random
# def GaussianNoise(src,means,sigma,percetage): 
# 	NoiseImg=src   
# 	NoiseNum=int(percetage*src.shape[0]*src.shape[1])  
# 	for i in range(NoiseNum):        
# 		randX=random.randint(0,src.shape[0]-1)      
# 		randY=random.randint(0,src.shape[1]-1)
# 		#此处在原有像素灰度值上加上随机数
# 		NoiseImg[randX,randY]=NoiseImg[randX,randY]+random.gauss(means,sigma)        
# 		#若灰度值小于0则强制为0，若灰度值大于255则强制为255
# 		if  NoiseImg[randX, randY]< 0:                 
# 			NoiseImg[randX, randY]=0        
# 		elif NoiseImg[randX, randY]>255:                 
# 			NoiseImg[randX, randY]=255   
# 	return NoiseImg

# img=cv2.imread('/work4/zhitiankai/infrared_noise_vad/data/spk_1_utt_1_0.jpg')
# img1=GaussianNoise(img,50,4,0.5)
# cv2.imwrite('/work4/zhitiankai/infrared_noise_vad/data/GaussianNoise.jpg',img1)
# # cv2.imshow('GaussianNoise',img1)
# cv2.waitKey(0)


# #方法2
# from skimage import util,io
# import cv2
# import numpy as np
# from PIL import Image

# img=cv2.imread('/work4/zhitiankai/infrared_noise_vad/data/spk_1_utt_1_0.jpg')
# # img = Image.open('/work4/zhitiankai/infrared_noise_vad/data/spk_1_utt_1_0.jpg')
# # img = np.array(img)

# noise_gs_img = util.random_noise(img,mode='gaussian') # gaussian 高斯加性噪声。
# noise_salt_img = util.random_noise(img,mode='salt')#盐噪声，随机用1替换像素。属于高灰度噪声。
# noise_pepper_img = util.random_noise(img,mode='pepper')# 胡椒噪声，随机用0或-1替换像素，属于低灰度噪声
# noise_sp_img = util.random_noise(img,mode='s&p') # 椒盐噪声，两种噪声同时出现，呈现出黑白杂点


# # noise_gs_img = (255*noise_gs_img).astype(np.uint8)
# # noise_gs_img = Image.fromarray(noise_gs_img)

# io.imsave('/work4/zhitiankai/infrared_noise_vad/data/GaussianNoise.jpg',noise_gs_img)
# # cv2.imwrite('/work4/zhitiankai/infrared_noise_vad/data/GaussianNoise.jpg',noise_gs_img)
# io.imsave('/work4/zhitiankai/infrared_noise_vad/data/salt.jpg',noise_salt_img)
# io.imsave('/work4/zhitiankai/infrared_noise_vad/data/pepper.jpg',noise_pepper_img)
# io.imsave('/work4/zhitiankai/infrared_noise_vad/data/sp.jpg',noise_sp_img)


#方法3
import numpy as np
import random
import cv2
import os

def sp_noise(image,prob):
    '''
    添加椒盐噪声
    prob:噪声比例
    '''
    output = np.zeros(image.shape,np.uint8)
    thres = 1 - prob
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()
            if rdn < prob:
                output[i][j] = 0
            elif rdn > thres:
                output[i][j] = 255
            else:
                output[i][j] = image[i][j]
    return output

def gasuss_noise(image, mean=0, var=0.001):
    '''
        添加高斯噪声
        mean : 均值
        var : 方差
    '''
    image = np.array(image/255, dtype=float)
    noise = np.random.normal(mean, var ** 0.5, image.shape)
    out = image + noise
    if out.min() < 0:
        low_clip = -1.
    else:
        low_clip = 0.
    out = np.clip(out, low_clip, 1.0)
    out = np.uint8(out*255)
    #cv.imshow("gasuss", out)
    return out

def create(data_path,dst_path):
    spks = os.listdir(data_path)
    imgs = []
    for i in spks:
        spk_path = os.path.join(dst_path,i)
        cmd = 'mkdir '+spk_path
        os.system(cmd)

    for j in spks:
        img_ids = os.listdir(os.path.join(data_path,j))
        for img_id in img_ids:
            img_path = os.path.join(data_path,j,img_id)
            imgs.append(img_path)

    return imgs

if __name__=="__main__":
    src_path = '/work4/zhitiankai/infrared_data_format/multi_modal_data/infrared_img'
    dst_path = '/work4/zhitiankai/infrared_noise_vad/nosie_data/inf_img_noise_0.008'
    imgs = create(src_path,dst_path)
    for i in imgs:
    # Read image
        img = cv2.imread(i)

        # 添加椒盐噪声，噪声比例为 0.02
        # out1 = sp_noise(img, prob=0.02)

        # 添加高斯噪声，均值为0，方差为0.001
        out2 = gasuss_noise(img, mean=0.2, var=0.008)

        # cv2.imwrite('/work4/zhitiankai/infrared_noise_vad/data/sp.jpg',out1)
        cv2.imwrite(dst_path+'/'+'/'.join(i.split('/')[-2:]),out2)