
import argparse
import numpy as np
from numpy.lib import ufunclike
from tqdm import tqdm
from random import shuffle
from sklearn.model_selection import train_test_split


import torch
import numpy as np
import torch.nn.functional as F


def compute_eer(target_scores, nontarget_scores): 
    if isinstance(target_scores , list) is False:
        target_scores = list(target_scores)
    if isinstance(nontarget_scores , list) is False:
        nontarget_scores = list(nontarget_scores)
    target_scores = sorted(target_scores)
    nontarget_scores = sorted(nontarget_scores)
    target_size = len(target_scores);
    nontarget_size = len(nontarget_scores)
    for i in range(target_size-1):
        target_position = i
        nontarget_n = nontarget_size * float(target_position) / target_size
        nontarget_position = int(nontarget_size - 1 - nontarget_n)
        if nontarget_position < 0:
            nontarget_position = 0
        if nontarget_scores[nontarget_position] < target_scores[target_position]:
            break
    th = target_scores[target_position];
    eer = target_position * 1.0 / target_size;
    print("th:{},cos-eer:{}".format(th,eer))
    return eer, th  

def data_normlize(data):
    return (data - data.mean(0)) / data.std(0)


def supervise_mean_var(data, label):
    assert(data.shape[0] == label.shape[0]), 'data and label must have the same length'  
    label_class = np.array(list(set(label.numpy())))
    label_class = torch.from_numpy(label_class)

    mean_list = []
    lb_list   = []
    for lb in label_class:
        data_j = data[label==lb]
        mean_j = torch.mean(data_j, 0, True) 
        mean_list.append(mean_j)
        lb_list.append(torch.tensor([lb]))
    class_mean  = torch.cat(mean_list, 0)
    class_label = torch.cat(lb_list)
    return class_mean, class_label



def Cosine(probe_data, probe_label, test_data, test_label):
    probe_data, probe_label = torch.from_numpy(probe_data), torch.from_numpy(probe_label)
    test_data,  test_label  = torch.from_numpy(test_data),  torch.from_numpy(test_label)

    class_mean, class_label = supervise_mean_var(probe_data, probe_label)
    
    Cosine_similaritys = []
    target_score = []
    no_target_score = []
    for i in range(len(test_data)): 
         Cosine_similarity = F.cosine_similarity(test_data[i].unsqueeze(0), class_mean)
         Cosine_similaritys.append(Cosine_similarity.unsqueeze(0))

         for j in range(len(class_label)):
            if test_label[i] == class_label[j]:
                target_score.append(Cosine_similarity.numpy().tolist()[j])
            else:
                no_target_score.append(Cosine_similarity.numpy().tolist()[j])
    
    Cos_matix     = torch.cat(Cosine_similaritys, 0)
    probe_index   = Cos_matix.argmax(dim=1)
    predict_label = torch.index_select(class_label, 0, probe_index)
    
    label_mask = torch.eq(predict_label, test_label)
    accuracy   = label_mask.sum() / len(label_mask)

    compute_eer(target_score,no_target_score)
    return float('%.4f' % accuracy.numpy())


def Dist(probe_data, probe_label, test_data, test_label):
    probe_data, probe_label = torch.from_numpy(probe_data), torch.from_numpy(probe_label)
    test_data,  test_label  = torch.from_numpy(test_data),  torch.from_numpy(test_label)

    dist_matix    = torch.cdist(test_data, probe_data, p=2)
    probe_index   = dist_matix.argmin(dim=1)
    predict_label = torch.index_select(probe_label, 0, probe_index)

    label_mask = torch.eq(predict_label, test_label)
    accuracy = label_mask.sum() / len(label_mask)

    return float('%.4f' % accuracy.numpy())



def enroll_test_split(Features, labels, n_enroll):
	zz=list(zip(Features, labels))
	shuffle(zz)

	enroll = []
	test	= []
	enroll_label = {}
	for i in zz:
		if i[1] not in enroll_label:
			enroll_label[i[1]] = 1
			enroll.append(i)

		elif enroll_label[i[1]] < n_enroll:
			enroll_label[i[1]] += 1
			enroll.append(i)

		else:
			test.append(i)

	enroll_data  = np.array(list(zip(*enroll))[0])
	enroll_label = np.array(list(zip(*enroll))[1])

	test_data  = np.array(list(zip(*test))[0])
	test_label = np.array(list(zip(*test))[1])

	return  enroll_data, enroll_label, test_data, test_label




if __name__ == "__main__":
# scoring settings
	parser = argparse.ArgumentParser(description='Thermal face baseline')
	parser.add_argument('--rang_erol',	type=int,	 default=10,     help='number of enroll samples')
	parser.add_argument('--score_data',	type=str,	 default='/work4/zhitiankai/infrared_data_format/multi_modal_add_noise_npz/inf_img_noise_0.014_asp_am.npz', help='path')
	args = parser.parse_args()

    #图像数据
	# data = np.load(args.score_data)
	# Features = data['img_vec']
	# labels = data['img_lab']

	data = np.load(args.score_data)
	Features = data['vectors']
	labels = data['labels']
	# labels = labels.tolist()
	# new_labels =[]
	# for i in labels:
	# 	j = int(i.split('_')[-1])
	# 	new_labels.append(j)
	# labels = np.array(new_labels)


    #声纹数据
	# data = np.load(args.score_data)
	# Features = data['vectors']
	# labels = data['labels']
	# tmp = [int(i) for i in labels]
	# labels = np.array(tmp)


	for n_enroll in range(1, args.rang_erol): #the number of enroll samples
           
		enroll_data, enroll_label, test_data, test_label = enroll_test_split(Features, labels, n_enroll)
		Cosine_accuracy	= Cosine(enroll_data, enroll_label, test_data, test_label)
		Distance_accuracy = Dist(enroll_data, enroll_label, test_data, test_label)
		
		print("Cos_acc:{},Distance_acc:{}".format(Cosine_accuracy,Distance_accuracy))