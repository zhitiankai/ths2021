import os
import random
import argparse
import re



def gen_ln(src_path,clean_path,dst_path):
    spk2utt = {}
    new_lines = []
    with open(clean_path,'r')as f:
        clean_lines = f.readlines()

    for i in clean_lines:
        new_lines.append('_'.join(i.split('_')[:2]))

    apply_spk = list(set(new_lines))
    for i in apply_spk:
        spk2utt[i] = []

    for i in spk2utt:
        for j in clean_lines:
            clean_spk = '_'.join(j.split('_')[:2])
            utt = j.strip().split('_')[-1].split(' ')[0]
            if i == clean_spk:
                spk2utt[i].append(utt)


    if not os.path.exists(dst_path):
        os.mkdir(dst_path)

    for spk in apply_spk:
        cmd = 'mkdir -p '+os.path.join(dst_path,spk)
        os.system(cmd)


    for src_spk in spk2utt:
        utt_list =  os.listdir(os.path.join(src_path,src_spk))
        if 'utt_list.txt' in utt_list:
            utt_list.remove('utt_list.txt')
        if 'utt_list_utf8.txt' in utt_list:
            utt_list.remove('utt_list_utf8.txt')
        for utt in utt_list:
            utt_id = utt.split('_')[1]
            if  utt_id in spk2utt[src_spk]:
                wav_list = os.listdir(os.path.join(src_path,src_spk,utt))
                if 'camera.avi' in wav_list:
                    wav_list.remove('camera.avi')
                if 'infared.avi' in wav_list:
                    wav_list.remove('infared.avi')
                wav_list.sort(key = lambda x:int(re.match('\D+(\d+)',x).group(1)))

                if len(wav_list)>0:
                    samples = wav_list

                    wav_path  = os.path.join(src_path,src_spk,utt,wav_list[0])
                    dst_img_path = os.path.join(dst_path,src_spk)+'/'+src_spk+'_utt_'+utt.split('_')[1]+'_'+wav_list[0]
                    cmd1 = 'ln -s '+wav_path+' '+dst_img_path
                    os.system(cmd1)
                else:
                    print("{}_{} not have 1 wav!!!".format(src_spk,utt))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='get data soft link!!!')
    parser.add_argument("--src_path", type=str, default='/work4/shenjingxin/data/InfraSpeech')
    parser.add_argument("--clean_path", type=str, default='/work4/zhitiankai/infrared_data_format/clean_list/multi_modal_list')
    parser.add_argument("--dst_path", type=str, default='/work4/zhitiankai/THS2021/voice_print/speaker_code/examples/THS2021/data')
    args = parser.parse_args()

    gen_ln(args.src_path,args.clean_path,args.dst_path)
