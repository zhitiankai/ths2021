#!/bin/bash

THS2021=/work4/zhitiankai/infrared_data_format/multi_modal_data/speech_data

nnet_type=ResNet34_half
pooling_type=ASP
loss_type=amsoftmax

vad=true
embedding_dim=256
save_xvector_path=/work4/zhitiankai/THS2021/voice_print/speaker_code/examples/THS2021

. ./path.sh

stage=1
echo stage $stage

# format data dir structure by soft link
if [ $stage -eq 0 ];then
	rm -rf data/wav_files
	mkdir -p data/wav_files

	# format THS2021
	ln -s ${THS2021}/* data/wav_files
fi


# build data list
if [ $stage -eq 1 ];then
	extension=wav
	data_dirs=data/wav_files
	
	if [[ $vad == true ]];then
		echo apply vad
		extension=vad
		data_dirs=data/vad_files
		python3 $SPEAKER_TRAINER_ROOT/scripts/vad.py --data_dir data/wav_files --num_jobs 40
		
	fi


	echo build dev data list
	python3 $SPEAKER_TRAINER_ROOT/scripts/build_datalist.py \
		--extension $extension \
		--dataset_dir $data_dirs \
		--data_list_path data/dev_list.csv

fi

# extract voice print
if [ $stage -eq 2 ];then
	ckpt_path=/work4/zhitiankai/infrared_voice_print/speaker_code/ResNet34_half_ASP_256_amsoftmax_0.2/epoch=46_train_loss=1.28.ckpt
	CUDA_VISIBLE_DEVICES=0,1 python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
		--batch_size 32 \
		--nnet_type $nnet_type \
		--pooling_type $pooling_type \
		--loss_type $loss_type \
		--num_workers 10 \
		--n_mels 80 \
		--train_list_path data/dev_list.csv \
		--gpus 2 \
		--max_frames 401 --min_frames 400 \
		--checkpoint_path $ckpt_path \
		--extract_vec \
		--embedding_dim $embedding_dim \
		--n_mels 80 \
		--save_xvector_path $save_xvector_path

	rm -rf lightning_logs
fi

