#!/bin/bash

voxceleb1_path=/work102/lilt/database/VoxCeleb/voxceleb1/
voxceleb2_path=/work102/lilt/database/VoxCeleb/voxceleb2/
musan_path=/work4/zhitiankai/dataset/musan
rirs_path=/work4/zhitiankai/dataset/RIRS_NOISES
trials_path=data/trials.lst

nnet_type=ResNet34_half 
pooling_type=ASP
loss_type=amsoftmax

#default false,no use vad
vad=true
embedding_dim=256

. ./path.sh

stage=1
echo stage $stage

# format data dir structure by soft link
if [ $stage -eq 0 ];then

	rm -rf data/wav_files/
	mkdir -p data/wav_files/

	# format voxceleb2
	ln -s ${voxceleb2_path}/dev/aac/* data/wav_files

	rm -rf voxceleb1_test_v2.txt
	wget https://openslr.magicdatatech.com/resources/49/voxceleb1_test_v2.txt
	mv voxceleb1_test_v2.txt data/voxceleb1_test_v2.txt
fi


# build data list
if [ $stage -eq 1 ];then
	extension=wav
	data_dirs=data/wav_files
	
	if [[ $vad == true ]];then
		echo apply vad
		extension=vad
		data_dirs=data/vad_files
		python3 $SPEAKER_TRAINER_ROOT/scripts/vad.py --data_dir data/wav_files --num_jobs 40
		
	fi

	echo build dev data list
	python3 $SPEAKER_TRAINER_ROOT/scripts/build_datalist.py \
		--extension $extension \
		--dataset_dir $data_dirs \
		--data_list_path data/dev_list.csv

	echo build musan data list
	python3 $SPEAKER_TRAINER_ROOT/scripts/build_datalist.py \
		--extension wav \
		--dataset_dir $musan_path \
		--data_list_path data/musan_list.csv

	echo build rirs data list
	python3 $SPEAKER_TRAINER_ROOT/scripts/build_datalist.py \
		--extension wav \
		--dataset_dir $rirs_path \
		--data_list_path data/rirs_list.csv

	rm -rf $trials_path
	python3 local/format_trials.py \
		--voxceleb1_root $voxceleb1_path \
		--src_trials_path data/voxceleb1_test_v2.txt \
		--dst_trials_path $trials_path
fi

# fast train
if [ $stage -eq 2 ];then
	CUDA_VISIBLE_DEVICES=0 python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
		--nnet_type $nnet_type \
		--loss_type $loss_type \
		--pooling_type $pooling_type \
		--batch_size 64 \
		--num_workers 80 \
		--n_mels 80 \
		--embedding_dim $embedding_dim \
		--save_top_k 50 \
		--train_list_path data/dev_list.csv \
		--musan_list_path data/musan_list.csv \
		--rirs_list_path data/rirs_list.csv \
		--augment \
		--max_epochs 50 \
		--max_frames 201 --min_frames 200 \
		--learning_rate 0.005 \
		--lr_step_size 5 \
		--lr_gamma 0.4 \
		--margin 0.2 \
		--distributed_backend dp \
		--trials_path $trials_path \
		--eval_interval 1 \
		--nPerSpeaker 1 \
		--reload_dataloaders_every_epoch \
		--gpus 1
fi


if [ $stage -eq 3 ];then
	ckpt_path=/work4/zhitiankai/infrared_voice_print/speaker_code/ResNet34_half_ASP_256_amsoftmax_0.2/epoch=45_train_loss=1.75.ckpt
	CUDA_VISIBLE_DEVICES=0 python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
		--batch_size 64 \
		--nnet_type $nnet_type \
		--pooling_type $pooling_type \
		--num_workers 100 \
		--n_mels 80 \
		--train_list_path data/dev_list.csv \
		--trials_path data/trials.lst \
		--gpus 1 \
		--max_frames 401 --min_frames 400 \
		--checkpoint_path $ckpt_path \
		--evaluate

	rm -rf lightning_logs
fi

