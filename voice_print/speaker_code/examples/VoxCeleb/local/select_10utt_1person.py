import numpy as np
import csv
import argparse

def select(path,path1):
    with open(path,'r') as f:    
        reader = csv.reader(f)
        column = [row[1] for row in reader]
    spk_column = list(set(column[1:]))

    spk_dict={}
    for i in spk_column:
        spk_dict[i]=[]

    with open(path,'r') as f1:
        txt = f1.readlines()

    for j in txt:
        spk = j.split(',')[1]
        if spk in spk_dict:
            spk_dict[spk].append(j)

    spk_choose={}
    for i in spk_dict:
        spk_choose[i]=spk_dict[i][:10]

    with open(path1,'w') as f:
        f.write(',speaker_name,utt_paths,utt_spk_int_labels\n')
        text = spk_choose.values()
        text = list(text)
        text = [j for i in text for j in i]
        for i in text:
            f.write(i)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--src_path', type=str, default="/work4/zhitiankai/THS2021/voice_print/speaker_code/examples/VoxCeleb/data/dev_list.csv")
    parser.add_argument('--dst_path', type=str, default="/work4/zhitiankai/THS2021/voice_print/speaker_code/examples/VoxCeleb/data/mini_dev_list.csv")
    args = parser.parse_args()
    select(args.src_path,args.dst_path)    

