#!/usr/bin/env python
# encoding: utf-8

from .utils import compute_eer
from .score import cosine_score
from .score import PLDA_score

