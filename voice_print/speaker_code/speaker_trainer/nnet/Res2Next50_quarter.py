#! /usr/bin/python
# -*- encoding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Parameter
try:
    from .Res2Next import *
except:
    from Res2Next import *

def Speaker_Encoder(embedding_dim=256, **kwargs):
    # Number of filters
    num_filters = [16, 32, 64, 128]
    model = Res2NeXt(Bottle2neckX, layers = [3, 4, 6, 3], baseWidth = 4, cardinality=8, scale = 4, num_filters=num_filters, embedding_dim=embedding_dim)
    return model

if __name__ == '__main__':
    model = Speaker_Encoder()
    total = sum([param.nelement() for param in model.parameters()])
    print(total/1e6)
    data = torch.randn(10, 64, 100)
    out = model(data)
    print(data.shape)
    print(out.shape)

